# Basic click event

When the button on the page is clicked, "Clicked" will be output in the console.

This is the [link to example](https://events-1-1-click-event.now.sh)

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. Add a button into the HTML
1. Select the button in JavaScript
1. Add the event listner on to the button
1. Create a callback function that is passed into the event listener.  The function should just output "click".